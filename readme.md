# Build

graalvm home must be set

```export GRAALVM_HOME=/home/martin/graalvm/graalvm-ce-1.0.0-rc13```

build native executable

```./mvnw clean package -Pnative```

build native executable inside a docker container
(for building linux executable on none linux systems)

```./mvnw clean package -Pnative -Dnative-image.docker-build=true```

run app

```./target/adder-quarkus-1.0-SNAPSHOT-runner```

## Using distroless Docker image

```docker build -t quarkus-quickstart/quickstart .```

run docker

```docker run -i --rm -p 8080:8080 quarkus-quickstart/quickstart```

## Using fedora-minimal Docker image (default for Quarkus) 

build the docker image

```docker build -f src/main/docker/Dockerfile.native -t quarkus-quickstart/quickstart .```

run docker

```docker run -i --rm -p 8080:8080 quarkus-quickstart/quickstart```
