
# libz is required but not in distroless. We copy it from debian:stable-slim
# https://github.com/GoogleContainerTools/distroless/issues/217
FROM debian:stable-slim AS build-env

FROM gcr.io/distroless/base

COPY --from=build-env /lib/x86_64-linux-gnu/libz.so.1 /lib/x86_64-linux-gnu/libz.so.1

COPY target/*-runner /application

EXPOSE 8080

CMD ["/application", "-Dquarkus.http.host=0.0.0.0"]
