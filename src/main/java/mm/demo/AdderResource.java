package mm.demo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/adder")
public class AdderResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public float add(@QueryParam("summand1") float summand1, @QueryParam("summand2") float summand2) {
        return summand1 + summand2;
    }

}
