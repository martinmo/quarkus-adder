package mm.demo;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

@QuarkusTest
public class AdderResourceTest {

    @Test
    public void testHelloEndpoint() {
        given()
                .when()
                .queryParam("summand1", "1")
                .queryParam("summand2", "2")
                .get("/adder")
                .then()
                .statusCode(200)
                .body(equalTo("3.0"));
    }

}